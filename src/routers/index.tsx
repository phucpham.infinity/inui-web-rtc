import {
  createBrowserRouter,
  RouterProvider as ReactRouterProvider,
} from "react-router-dom";

import AppLayout from "@/layout/app.layout";
import MainPage from "@/pages/main";
import RoomPage from "@/pages/room";
import DemoPage from "@/pages/demo";

const router = createBrowserRouter([
  {
    path: "/",
    element: <AppLayout />,
    children: [
      {
        path: "/",
        element: <MainPage />,
      },
      {
        path: "/demo",
        element: <DemoPage />,
      },
      {
        path: "/room/:id",
        element: <RoomPage />,
      },
    ],
  },
]);

const RouterProvider = () => {
  return <ReactRouterProvider router={router} />;
};

export default RouterProvider;
