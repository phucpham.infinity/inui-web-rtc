import { useEffect, useState, createContext, useContext } from "react";
import { SupabaseClient, createClient } from "@supabase/supabase-js";
import { Auth } from "@supabase/auth-ui-react";
import { ThemeSupa } from "@supabase/auth-ui-shared";
import { Box, Center } from "@chakra-ui/layout";

const supabase = createClient(
  "https://utopxtvdnwefotehycdm.supabase.co",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InV0b3B4dHZkbndlZm90ZWh5Y2RtIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODEzMTQ2MTYsImV4cCI6MTk5Njg5MDYxNn0.O0ebznL92kDokvWababo3kCYJtUNzmZUf2g4viP6YvU"
);

export interface ISupabaseContext {
  supabase: SupabaseClient;
}

export const SupabaseContext = createContext<ISupabaseContext | null>({
  supabase,
});

export default function SupabaseProvider({
  children,
}: {
  children: React.ReactElement;
}) {
  const [session, setSession] = useState(null);

  useEffect(() => {
    supabase.auth.getSession().then(({ data: { session } }) => {
      setSession(session);
    });

    const {
      data: { subscription },
    } = supabase.auth.onAuthStateChange((_event, session) => {
      setSession(session);
    });

    return () => subscription.unsubscribe();
  }, []);

  if (!session) {
    return (
      <Center h={"100vh"} w={"100vw"}>
        <Box w={"30%"}>
          <Auth
            supabaseClient={supabase}
            providers={[]}
            appearance={{ theme: ThemeSupa }}
          />
        </Box>
      </Center>
    );
  } else {
    return (
      <SupabaseContext.Provider value={{ supabase }}>
        {children}
      </SupabaseContext.Provider>
    );
  }
}

export const useSupabaseContext = () => {
  return useContext(SupabaseContext) as ISupabaseContext;
};
