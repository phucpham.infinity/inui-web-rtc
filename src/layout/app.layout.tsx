import { Outlet } from "react-router-dom";
import NavBar from "@/components/navbar";
import DrawerProvider from "@/components/drawer";
import InitVideoCall from "@/components/video-call";
import MyVideo from "@/components/my-video";

function App() {
  return (
    <DrawerProvider>
      <NavBar />
      <InitVideoCall />
      <MyVideo />
      <Outlet />
    </DrawerProvider>
  );
}

export default App;
