import React from "react";
import ReactDOM from "react-dom/client";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { Helmet } from "react-helmet";
import { WSProvider } from "@/context";
import SupabaseProvider from "@/services/supabase";
import RouterProvider from "@/routers";

const colors = {
  brand: {
    50: "#ecefff",
    100: "#cbceeb",
    200: "#a9aed6",
    300: "#888ec5",
    400: "#666db3",
    500: "#4d5499",
    600: "#3c4178",
    700: "#2a2f57",
    800: "#181c37",
    900: "#080819",
  },
};

const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
};

const theme = extendTheme({ colors, config });

ReactDOM.createRoot(document.getElementById("root")!).render(
  <WSProvider url="signaling">
    <ChakraProvider theme={theme} resetCSS>
      <Helmet />
      <React.StrictMode>
        <SupabaseProvider>
          <RouterProvider />
        </SupabaseProvider>
      </React.StrictMode>
    </ChakraProvider>
  </WSProvider>
);
