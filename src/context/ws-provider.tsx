import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { debounce } from "lodash";

export interface IWSStateContext {
  ws: WebSocket | null;
  sendMessage: (message: any) => any | null;
  registerState: REGISTER_STATES | null;
  callState: CALL_STATE | null;
  wsState: WS_STATE | null;
  register: (username: string) => any;
  call: (args: { peer: Array<string | number>; videoInput: any }) => any;
  onIceCandidate: (name: string) => any;
  incomingCallData: { from: string; roomId: string } | null;
  isIncomingCall: boolean;
  onDeclineIncomingCall: () => any;
  onAcceptIncomingCall: (prams?: any) => any;
  error?: any;
  roomId?: string;
  stop?: () => any;
  peer?: string[] | null;
  message?: any;
  receiveMediaFrom?: ({
    username,
    remoteVideo,
    roomId,
  }: {
    username: string;
    remoteVideo: any;
    roomId: string;
  }) => any;
  roomPeer: any[];
}
export type WSProviderProps = { children: ReactNode; url: string };

export enum REGISTER_STATES {
  "NOT_REGISTERED" = 0,
  "REGISTERING" = 1,
  "REGISTERED" = 2,
}

export enum CALL_STATE {
  "NO_CALL" = 0,
  "PROCESSING_CALL" = 1,
  "IN_CALL" = 2,
}

export enum WS_STATE {
  "CONNECTING" = WebSocket.CONNECTING,
  "OPEN" = WebSocket.OPEN,
  "CLOSING" = WebSocket.CLOSING,
  "CLOSED" = WebSocket.CLOSED,
}

function delay(seconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds);
  });
}

const WSStateContext = createContext<IWSStateContext | null>(null);

export const WSProvider = ({ children, url }: WSProviderProps): JSX.Element => {
  const wsInstance = useRef<WebSocket | null>(null);

  useEffect(() => {
    if (!wsInstance.current)
      wsInstance.current = new WebSocket("wss://media-demo.vinhnd.dev/" + url);

    return () => {
      if (wsInstance.current?.readyState === WS_STATE.OPEN)
        wsInstance.current?.close();
    };
  }, []);

  useEffect(() => {
    if (wsInstance.current) {
      wsInstance.current.onmessage = handleOnmessage;
      wsInstance.current.addEventListener("open", () => {
        if (wsInstance.current?.readyState === WS_STATE.OPEN)
          setWsState(WS_STATE.OPEN);
      });
      wsInstance.current.addEventListener("close", () => {
        if (wsInstance.current?.readyState === WS_STATE.CLOSED)
          // setWsState(WS_STATE.CLOSED);
          wsInstance.current = new WebSocket(
            "wss://media-demo.vinhnd.dev/" + url
          );
      });
    }
  }, [wsInstance]);

  const username = useRef("");
  const [wsState, setWsState] = useState<WS_STATE | null>(null);
  const [registerState, setRegisterState] = useState<REGISTER_STATES | null>(
    null
  );

  const [callState, setCallState] = useState<CALL_STATE | null>(
    CALL_STATE.NO_CALL
  );

  const myPeer = useRef<any>(null);
  const partnerPeers = useRef<any>({});
  const myAccount = useRef<any>(null);

  const [message, setMessage] = useState<any>(null);
  const [incomingCallData, setIncomingCallData] = useState<{
    from: string;
    roomId: string;
  } | null>(null);

  const [isIncomingCall, setIsIncomingCall] = useState(false);
  const [error, setError] = useState<any>(null);
  const [roomId, setRoomId] = useState<string | null>(null);
  const [peer, setPeer] = useState<string[] | null>(null);

  const stop = () => {
    sendMessage({
      id: "stop",
    });
    setCallState(null);
    setIncomingCallData(null);
    setIsIncomingCall(false);
    setMessage(null);
    setRoomId(null);
    myAccount.current = null;
    myPeer.current = null;
    partnerPeers.current = {};
    setCallState(CALL_STATE.NO_CALL);
  };

  const unregister = () => {
    sendMessage({
      id: "unregister",
    });
  };

  useEffect(() => {
    return () => {
      stop();
    };
  }, []);

  const sendMessage = (message: any) => {
    const jsonMessage = JSON.stringify(message);
    if (wsInstance.current?.readyState === WS_STATE.OPEN) {
      wsInstance?.current?.send?.(jsonMessage);
    }
  };

  const handleOnmessage = (message: MessageEvent<any>) => {
    const parsedMessage = JSON.parse(message.data) || {};
    setMessage(parsedMessage);
    switch (parsedMessage.id) {
      case "registerResponse":
        registerResponse(parsedMessage);
        break;
      case "callResponse":
        callResponse(parsedMessage);
        break;
      case "beginSendMedia":
        if (myPeer.current) {
          myPeer.current.processAnswer(parsedMessage.sdpAnswer);
        }
        break;

      case "incomingCall":
        incomingCall(parsedMessage);
        break;
      case "startCommunication":
        startCommunication(parsedMessage);
        break;
      case "receiveMediasFrom":
        receiveMediasFrom(parsedMessage);
        break;
      case "iceCandidate":
        iceCandidateHandler(parsedMessage);
        break;
      case "stopCommunication":
        stop();
        break;
      default:
        console.error("Unrecognized message", parsedMessage);
    }
  };

  const iceCandidateHandler = (message: any) => {
    if (message.userName === username.current) {
      if (myPeer.current) myPeer.current.addIceCandidate(message.candidate);
    } else {
      partnerPeers.current[message.userName]?.addIceCandidate(
        message.candidate
      );
    }
  };

  const registerResponse = (message: any) => {
    if (message.response === "accepted") {
      setRegisterState(REGISTER_STATES.REGISTERED);
    } else {
      setRegisterState(REGISTER_STATES.NOT_REGISTERED);
      const errorMessage = message.message
        ? message.message
        : "Unknown reason for register rejection.";
      console.error(errorMessage);
    }
  };

  const [roomPeer, setRoomPeer] = useState({});

  const callResponse = (message: any) => {
    if (message.response === "accept") {
      setRoomId(message.roomId);
      setRoomPeer((s) => ({
        ...s,
        [message.userName]: {
          status: message.response,
          username: message.userName,
          roomId: message.roomId,
        },
      }));
    } else {
      setError(message);
      stop();
    }
  };

  const receiveMediaFrom = (payload: {
    username: string;
    remoteVideo: any;
    roomId: string;
  }) => {
    const { username, remoteVideo, roomId } = payload;
    if (roomPeer[username].status === "received") return;
    setRoomPeer((s) => ({
      ...s,
      [username]: {
        status: "received",
        username,
        roomId,
      },
    }));
    const options = {
      remoteVideo: remoteVideo,
      onicecandidate: onIceCandidate(username),
      configuration: {
        iceServers: [
          {
            url: "turn:103.56.163.217:3478",
            username: "kurento",
            credential: "kurento",
          },
        ],
      },
    };
    partnerPeers.current[username] =
      //@ts-ignore
      kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function (error) {
        if (error) {
          console.log(error);
          return;
        }
        //@ts-ignore
        this.generateOffer((error, sdp) => {
          const response = {
            id: "receiveMediaFrom",
            roomId,
            remoteId: username,
            sdpOffer: sdp,
          };
          sendMessage(response);
        });
      });
  };

  const startCommunication = (message: any) => {
    setCallState(CALL_STATE.IN_CALL);
    partnerPeers.current[message.userName].processAnswer(message.sdpAnswer);
  };

  const register = (name: string) => {
    if (registerState !== null) return;
    username.current = name;
    if (registerState === null) {
      setRegisterState(REGISTER_STATES.REGISTERING);
      const message = {
        id: "register",
        name: name,
      };
      sendMessage(message);
      setRegisterState(REGISTER_STATES.REGISTERED);
    }
  };

  const onIceCandidate = (name: string) => {
    return (candidate: any) => {
      const message = {
        id: "onIceCandidate",
        candidate: candidate,
        name,
      };
      sendMessage(message);
    };
  };

  const incomingCall = (message: any) => {
    // If busy just reject without disturbing user
    if (callState === CALL_STATE.IN_CALL) {
      return sendMessage({
        id: "incomingCallResponse",
        roomId: message.roomId,
        callResponse: "reject",
        message: "busy",
      });
    } else {
      setIsIncomingCall(true);
      setIncomingCallData({
        from: message.from,
        roomId: message.roomId,
      });
    }
  };
  const onAcceptIncomingCall = ({ videoInput }: any) => {
    if (callState === CALL_STATE.PROCESSING_CALL) return;
    setCallState(CALL_STATE.PROCESSING_CALL);
    const options = {
      localVideo: videoInput,
      mediaConstraints: {
        audio: true,
        video: {
          mandatory: {
            maxWidth: 1080,
            maxFrameRate: 15,
            minFrameRate: 15,
          },
        },
      },
      onicecandidate: onIceCandidate(username.current),
      configuration: {
        iceServers: [
          {
            url: "turn:103.56.163.217:3478",
            username: "kurento",
            credential: "kurento",
          },
        ],
      },
    };
    myPeer.current =
      //@ts-ignore
      kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function (error) {
        if (error) {
          console.log(error);
          return;
        }
        this.generateOffer((error, sdp) => {
          var response = {
            id: "incomingCallResponse",
            roomId: incomingCallData.roomId,
            callResponse: "accept",
            sdpOffer: sdp,
          };
          sendMessage(response);
        });
      });
  };
  const onDeclineIncomingCall = () => {
    if (!incomingCallData) return;
    sendMessage({
      id: "incomingCallResponse",
      roomId: incomingCallData.roomId,
      callResponse: "reject",
      message: "user declined",
    });
    setIsIncomingCall(false);
    setIncomingCallData(null);
  };
  const receiveMediasFrom = async (message: any) => {
    setRoomId(message.roomId);
    myPeer.current?.processAnswer(message.sdpAnswer);
    const participants = message?.participants.filter(
      (x) => x.name !== "phuc.pxp@gmail.com"
    );
    for (let i = 0; i < participants?.length; i++) {
      const item = participants[i];
      console.log("set =>>>>", item);
      setRoomPeer((s) => ({
        ...s,
        [item.name]: {
          roomId: message.roomId,
          username: item.name,
          status: "accepted",
        },
      }));
      await delay(300);
    }

    // setParticipants(
    //   message?.participants.filter((u) => u.name !== username.current)
    // );
  };

  // useEffect(() => {
  //   if (participants && participants.length) {
  //     participants.forEach((user: any) => {
  //       const options = {
  //         remoteVideo: partnerVideos.current[user.name],
  //         onicecandidate: onIceCandidate(user.name),
  //         configuration: {
  //           iceServers: [
  //             {
  //               url: "turn:103.56.163.217:3478",
  //               username: "kurento",
  //               credential: "kurento",
  //             },
  //           ],
  //         },
  //       };
  //       partnerPeers.current[user.name] =
  //         //@ts-ignore
  //         kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function (error) {
  //           if (error) {
  //             console.log(error);
  //             return;
  //           }
  //           this.generateOffer((error, sdp) => {
  //             var response = {
  //               id: "receiveMediaFrom",
  //               roomId: incomingCallData.roomId,
  //               remoteId: user.name,
  //               sdpOffer: sdp,
  //             };
  //             sendMessage(response);
  //           });
  //         });
  //     });
  //   }
  // }, [participants]);

  const call = async ({
    peer,
    videoInput,
  }: {
    peer: string[];
    videoInput: any;
  }) => {
    if (callState === CALL_STATE.PROCESSING_CALL) return;
    setCallState(CALL_STATE.PROCESSING_CALL);
    setPeer(peer);
    const options = {
      localVideo: videoInput,
      mediaConstraints: {
        audio: true,
        video: {
          mandatory: {
            maxWidth: 1080,
            maxFrameRate: 30,
            minFrameRate: 15,
          },
        },
      },
      onicecandidate: onIceCandidate(username.current),
      configuration: {
        iceServers: [
          {
            url: "turn:103.56.163.217:3478",
            username: "kurento",
            credential: "kurento",
          },
        ],
      },
    };

    //@ts-ignore
    myPeer.current = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(
      options,
      function (error) {
        if (error) {
          console.log(error);
          return;
        }
        // @ts-ignore
        this.generateOffer((error, offerSdp) => {
          const response = {
            id: "call",
            from: username.current,
            to: peer,
            sdpOffer: offerSdp,
          };
          sendMessage(response);
        });
      }
    );
  };

  useEffect(() => {
    if (wsState === WS_STATE.CLOSED) {
    }
  }, [wsState]);

  return (
    <WSStateContext.Provider
      value={{
        ws: wsInstance.current,
        sendMessage,
        registerState,
        callState,
        wsState,
        isIncomingCall,
        incomingCallData,
        error,
        roomId,
        peer,
        message,
        roomPeer: Object.values(roomPeer),
        register: debounce(register, 300),
        call: debounce(call, 300),
        onIceCandidate: debounce(onIceCandidate, 300),
        onAcceptIncomingCall: debounce(onAcceptIncomingCall, 300),
        onDeclineIncomingCall: debounce(onDeclineIncomingCall, 300),
        receiveMediaFrom: debounce(receiveMediaFrom, 300),
        stop: debounce(stop, 300),
      }}
    >
      {children}
    </WSStateContext.Provider>
  );
};

export const useWS = (): IWSStateContext => {
  const context = useContext(WSStateContext);
  if (context == undefined) {
    throw new Error("useWS must be used within a WSProvider");
  }
  return context;
};
