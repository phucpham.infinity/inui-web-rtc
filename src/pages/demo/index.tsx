import { Box, HStack, Text } from "@chakra-ui/react";
import React from "react";
import useDimensions from "react-cool-dimensions";

const demo: React.FC = () => {
  const { observe, width, height } = useDimensions();

  return (
    <HStack ref={observe} bg={"red"} h="calc(100vh - 80px)" w={"100vw"}>
      <Box w={width / 2}>
        <Text color={"white"}>1</Text>
      </Box>
      <Box w={width / 2}>
        <Text color={"white"}>2</Text>
      </Box>
    </HStack>
  );
};

export default demo;
