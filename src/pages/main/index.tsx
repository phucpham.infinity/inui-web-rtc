import * as CK from "@chakra-ui/react";
import Contacts from "@/components/contacts";

function Main() {
  return (
    <CK.Stack p={5}>
      <Contacts />
    </CK.Stack>
  );
}

export default Main;
