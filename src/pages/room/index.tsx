import { useEffect, useRef, memo, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Center, GridItem, HStack, Text } from "@chakra-ui/react";
import { isEmpty } from "lodash";

import { CALL_STATE, useWS } from "@/context";
import { useSupabaseContext } from "@/services/supabase";
import VoiceBar from "@/components/voice-bar";

interface IPeerVideo {
  username?: string;
  roomId?: string;
  status?: string;
  height?: string;
}

const PeerVideo = memo((props: IPeerVideo) => {
  const { username, roomId, status, height } = props;
  const { receiveMediaFrom } = useWS();
  const videoEL = useRef<any>(null);
  useEffect(() => {
    if (videoEL.current) {
      if (status === "received") return;
      receiveMediaFrom({
        username,
        remoteVideo: videoEL.current,
        roomId,
      });
    }
  }, [videoEL]);

  return (
    <GridItem>
      <Center position={"relative"}>
        <Text
          w={"100%"}
          zIndex={2}
          left={"0px"}
          top={"30px"}
          height={"40px"}
          position={"absolute"}
          p={2}
          fontSize="18px"
        >
          {username}
        </Text>
        <video
          poster={`https://i.pravatar.cc/500?u=${username}`}
          id={`videoPeer-${username}`}
          ref={videoEL}
          autoPlay
          style={{
            width: "100%",
            height,
            transform: "scaleX(-1)",
            borderRadius: "10px",
          }}
          key={username}
        ></video>
      </Center>
    </GridItem>
  );
});

const Room = () => {
  const { roomPeer, callState } = useWS();
  const navigate = useNavigate();
  useEffect(() => {
    document.getElementById("myVideo").style.zIndex = "99";
    document.getElementById("myVideo").style.opacity = "1";
  }, []);

  useEffect(() => {
    return () => {
      document.getElementById("myVideo").style.zIndex = "-1";
      document.getElementById("myVideo").style.opacity = "0";
    };
  }, []);

  useEffect(() => {
    if (callState === CALL_STATE.NO_CALL) {
      navigate("/");
      setTimeout(() => {
        location.reload();
      }, 200);
    }
  }, [callState]);

  const { supabase } = useSupabaseContext();
  const [user, setUser] = useState(null);

  useEffect(() => {
    supabase.auth.getUser().then((res) => {
      setUser(res.data?.user);
    });
  }, []);

  return (
    <HStack
      justifyContent={"space-around"}
      alignItems={"center"}
      p={8}
      gap={5}
      flexWrap={"wrap"}
      h={"calc(100vh - 80px)"}
    >
      {!isEmpty(roomPeer) &&
        !!user &&
        roomPeer
          .filter((x) => x.username !== user.email)
          .map((peer) => (
            <PeerVideo
              roomId={peer.roomId}
              key={peer.username}
              username={peer.username}
              status={peer.status}
              height="calc(100vh - 150px)"
            />
          ))}
      <VoiceBar />
    </HStack>
  );
};

export default Room;
