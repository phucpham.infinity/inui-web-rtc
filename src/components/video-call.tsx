import * as CK from "@chakra-ui/react";
import IncomingCallModal from "@/components/incoming-call";

import { WS_STATE, useWS } from "@/context";
import { useEffect } from "react";
import { useSupabaseContext } from "@/services/supabase";
import { useNavigate } from "react-router-dom";
import { useToast } from "@chakra-ui/react";

function InitVideoCall() {
  const {
    wsState,
    register,
    isIncomingCall,
    onDeclineIncomingCall,
    onAcceptIncomingCall,
    incomingCallData,
    roomId,
  } = useWS();
  const { supabase } = useSupabaseContext();
  const { isOpen, onClose, onOpen } = CK.useDisclosure();
  const navigate = useNavigate();
  const toast = useToast();

  useEffect(() => {
    if (supabase && wsState === WS_STATE.OPEN) {
      supabase.auth.getSession().then(({ data }) => {
        if (data?.session?.user?.email) {
          register(data?.session?.user?.email);
        }
      });
    }
  }, [supabase, wsState]);

  useEffect(() => {
    if (wsState === WS_STATE.OPEN) {
      toast({
        title: "Web socket",
        description: "We've connected your web socket for you.",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    }
    if (wsState === WS_STATE.CLOSED) {
      toast({
        title: "Web socket",
        description: "We've disconnected your web socket for you.",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  }, [wsState]);

  useEffect(() => {
    if (isIncomingCall) {
      onOpen();
    } else {
      onClose();
    }
  }, [isIncomingCall]);

  const handleCloseInComingCall = () => {
    onClose();
  };

  useEffect(() => {
    if (roomId) {
      onClose();
      navigate(`/room/${roomId}`);
    }
  }, [roomId]);

  return (
    <IncomingCallModal
      isOpen={isOpen}
      onClose={handleCloseInComingCall}
      data={incomingCallData}
      onAccept={() => {
        onAcceptIncomingCall({
          videoInput: document.getElementById("myVideo"),
        });
      }}
      onDecline={onDeclineIncomingCall}
    />
  );
}

export default InitVideoCall;
