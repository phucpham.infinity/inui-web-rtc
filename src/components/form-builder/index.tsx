import { forwardRef, useImperativeHandle } from "react";
import { useForm } from "react-hook-form";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Grid,
} from "@chakra-ui/react";

interface IForm {
  formData?: {
    name: string;
    type: "text";
    label: string;
    validate: any;
  }[];
  onSubmit?: (payload: any) => any;
  templateColumns?: string;
  gap?: number;
}

function Form(props: IForm, ref: any) {
  const {
    formData,
    onSubmit,
    templateColumns = "repeat(1, 1fr)",
    gap = 3,
  } = props;
  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm();

  useImperativeHandle(
    ref,
    () => ({
      submit: handleSubmit(onSubmit),
      getValues: getValues,
    }),
    []
  );

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid templateColumns={templateColumns} gap={gap}>
        {formData.map((field, index) => (
          <FormControl key={index} isInvalid={errors[field.name] as any}>
            <FormLabel htmlFor={field.name}>{field.label}</FormLabel>
            <Input
              id={field.name}
              type={field.type}
              {...register(field.name, { validate: field.validate })}
            />
            <FormErrorMessage>
              {/* @ts-ignore */}
              {errors[field.name] && errors[field.name].message}
            </FormErrorMessage>
          </FormControl>
        ))}
      </Grid>
    </form>
  );
}

export default forwardRef(Form);
