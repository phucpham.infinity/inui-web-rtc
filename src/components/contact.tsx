import {
  Box,
  Center,
  Flex,
  HStack,
  IconButton,
  Image,
  Link,
  Text,
  VStack,
  chakra,
  useDisclosure,
} from "@chakra-ui/react";
import { MdClose, MdLocalPhone, MdMessage } from "react-icons/md";
import CallLoading from "@/components/call-loading";

interface IContact {
  email: string;
  name: string;
  id: string;
  onRemove?: (id: string) => any;
  type: string;
  users: any;
}

export default function Contact(props: IContact) {
  const { email, name, onRemove, id, type, users } = props;
  const { isOpen, onClose, onOpen } = useDisclosure();

  return (
    <Box
      w="xs"
      bg="white"
      _dark={{ bg: "gray.700" }}
      shadow="lg"
      rounded="lg"
      overflow="hidden"
      mx="auto"
      position={"relative"}
    >
      <CallLoading
        isOpen={isOpen}
        name={name}
        email={email}
        onClose={onClose}
        peer={type === "USER" ? [email] : users.map((x: any) => x.email)}
      />
      <IconButton
        isRound={true}
        aria-label="Search database"
        icon={<MdClose />}
        position={"absolute"}
        right={3}
        top={3}
        onClick={() => onRemove?.(id)}
      />
      <Image
        w="full"
        h={56}
        fit="cover"
        src={`https://i.pravatar.cc/600?u=${email}`}
        alt="avatar"
      />

      <Box py={5} textAlign="center">
        <Link
          display="block"
          fontSize="2xl"
          color="gray.800"
          _dark={{ color: "white" }}
          fontWeight="bold"
        >
          {name}
        </Link>
        {type === "USER" && (
          <chakra.span
            fontSize="sm"
            color="gray.700"
            _dark={{ color: "gray.200" }}
          >
            {email}
          </chakra.span>
        )}
        {type === "GROUP" && (
          <chakra.span
            fontSize="sm"
            color="gray.700"
            _dark={{ color: "gray.200" }}
          >
            <HStack alignItems={"center"} justifyContent={"center"}>
              {users.map((x) => (
                <Text key={x.id}>{x.name}</Text>
              ))}
            </HStack>
          </chakra.span>
        )}
      </Box>
      <Center pb={4}>
        <HStack spacing={4}>
          <VStack>
            <IconButton
              sx={{ fontSize: "20px" }}
              isRound={true}
              variant="solid"
              colorScheme="green"
              aria-label="Search database"
              onClick={() => {
                onOpen();
              }}
              icon={<MdLocalPhone />}
            />
            <Text>Call</Text>
          </VStack>
          <VStack>
            <IconButton
              sx={{ fontSize: "20px" }}
              isRound={true}
              variant="solid"
              colorScheme="blue"
              aria-label="Search database"
              icon={<MdMessage />}
            />
            <Text>Message</Text>
          </VStack>
        </HStack>
      </Center>
    </Box>
  );
}
