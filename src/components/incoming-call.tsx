import {
  Avatar,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  VStack,
  IconButton,
  HStack,
} from "@chakra-ui/react";
import { MdClose, MdLocalPhone } from "react-icons/md";

interface IIncomingCall {
  isOpen?: boolean;
  onClose?: () => any;
  onAccept?: () => any;
  onDecline?: () => any;
  data?: {
    from: string;
    roomId: string;
  };
}

const IncomingCall = ({
  isOpen,
  onClose,
  data,
  onAccept,
  onDecline,
}: IIncomingCall) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />
        <ModalBody>
          <VStack my={4}>
            <Avatar
              name={data?.from}
              src={`https://i.pravatar.cc/500?u=${data?.from}`}
            />
            <Text
              lineHeight={"28px"}
              textAlign={"center"}
              fontSize={"3xl"}
              fontWeight={600}
            >
              {data?.from} is calling for you
            </Text>
            <Text color={"gray"} fontSize={"md"} fontWeight={500}>
              The call will start as soon as you accept
            </Text>
            <HStack mt={5} gap={4}>
              <VStack>
                <IconButton
                  isRound={true}
                  variant="solid"
                  colorScheme="red"
                  aria-label="Search database"
                  onClick={onDecline}
                  icon={<MdClose />}
                />
                <Text>Decline</Text>
              </VStack>
              <VStack>
                <IconButton
                  isRound={true}
                  onClick={onAccept}
                  variant="solid"
                  colorScheme="green"
                  aria-label="Search database"
                  icon={<MdLocalPhone />}
                />
                <Text>Accept</Text>
              </VStack>
            </HStack>
          </VStack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default IncomingCall;
