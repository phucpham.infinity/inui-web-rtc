import React from "react";
import { Box, Circle, HStack } from "@chakra-ui/react";
import { ImPhoneHangUp } from "react-icons/im";
import {
  BsMicMuteFill,
  BsFillCameraVideoOffFill,
  BsPersonFillAdd,
} from "react-icons/bs";
import { useWS } from "@/context";
import { useNavigate } from "react-router-dom";

const VoiceBar: React.FC = () => {
  const { stop } = useWS();
  const navigate = useNavigate();
  return (
    <HStack
      sx={{
        position: "fixed",
        w: "300px",
        zIndex: 99,
        height: "60px",
        bgColor: "white",
        bottom: "30px",
        borderRadius: "100px",
        boxShadow: "0 .5rem 1rem rgba(0,0,0,.15)",
        fontSize: "30px",
      }}
      justifyContent={"space-around"}
      alignItems={"center"}
    >
      <Circle p={1.5} cursor={"pointer"}>
        <BsPersonFillAdd />
      </Circle>
      <Circle p={1.5} color={"#717171"} cursor={"pointer"}>
        <BsMicMuteFill />
      </Circle>
      <Circle p={1.5} color={"#717171"} cursor={"pointer"}>
        <BsFillCameraVideoOffFill />
      </Circle>
      <Circle
        onClick={() => {
          stop();
          navigate("/");
          setTimeout(() => {
            location.reload();
          }, 300);
        }}
        p={1.5}
        fontSize={"34px"}
        cursor={"pointer"}
        color={"#ff4942"}
      >
        <ImPhoneHangUp />
      </Circle>
    </HStack>
  );
};

export default VoiceBar;
