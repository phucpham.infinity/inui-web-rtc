import { useEffect, useState } from "react";
import Contact from "./contact";
import { useSupabaseContext } from "@/services/supabase";
import { Center, Grid, Spinner, Text } from "@chakra-ui/react";
import { isEmpty } from "lodash";

const Contacts = () => {
  const { supabase } = useSupabaseContext();
  const [contacts, setContacts] = useState<any[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchContacts = async () => {
    setIsLoading(true);
    const { data } = await supabase.auth.getSession();
    const { data: _contacts, error } = await supabase
      .from("contacts")
      .select("*")
      .eq("user_id", data.session.user.id);
    if (!error) setContacts(_contacts);
    setIsLoading(false);
  };
  const refetchContacts = async () => {
    const { data } = await supabase.auth.getSession();
    const { data: _contacts, error } = await supabase
      .from("contacts")
      .select("*")
      .eq("user_id", data.session.user.id);
    if (!error) setContacts(_contacts);
  };
  useEffect(() => {
    fetchContacts();
    const channels = supabase
      .channel("custom-all-channel")
      .on(
        "postgres_changes",
        { event: "*", schema: "public", table: "contacts" },
        () => {
          refetchContacts();
        }
      )
      .subscribe();
    return () => {
      channels.unsubscribe();
    };
  }, []);

  const handleRemoveContact = async (id: string) => {
    const { error } = await supabase.from("contacts").delete().eq("id", id);
    if (error) {
    }
  };

  return isLoading ? (
    <Center h={"10vh"}>
      <Spinner />
    </Center>
  ) : (
    <Grid
      templateColumns="repeat(4, 1fr)"
      gap={6}
      bg="#edf3f8"
      _dark={{ bg: "transparent" }}
      p={50}
      w="full"
      alignItems="center"
      justifyContent="center"
      borderRadius={"10px"}
    >
      {isEmpty(contacts) ? (
        <Center w={"100vw"}>
          <Text>Contact empty!</Text>
        </Center>
      ) : (
        contacts?.map((x) => (
          <Contact
            onRemove={handleRemoveContact}
            key={x.id}
            id={x.id}
            name={x.name}
            email={x.email}
            type={x.type}
            users={x.users}
          />
        ))
      )}
    </Grid>
  );
};

export default Contacts;
