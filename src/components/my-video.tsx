import React from "react";

interface IMyVideo {
  opacity?: number;
  zIndex?: number;
}

function MyVideo(props: IMyVideo) {
  const { opacity = 0, zIndex = -1 } = props;
  return (
    <video
      id="myVideo"
      autoPlay
      muted
      poster="https://i.pravatar.cc/500?u=4"
      style={{
        width: "260px",
        height: "auto",
        transform: "scaleX(-1)",
        borderRadius: "10px",
        position: "fixed",
        bottom: "20px",
        right: "20px",
        opacity,
        zIndex,
      }}
    ></video>
  );
}

export default MyVideo;
