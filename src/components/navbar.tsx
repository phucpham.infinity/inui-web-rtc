import React, { useEffect, useState } from "react";

import {
  chakra,
  Box,
  Flex,
  useColorModeValue,
  VisuallyHidden,
  HStack,
  Button,
  useDisclosure,
  VStack,
  IconButton,
  CloseButton,
  Avatar,
  Menu,
  MenuList,
  MenuItem,
  MenuButton,
  MenuDivider,
  Text,
} from "@chakra-ui/react";
import { AiOutlineMenu, AiFillHome, AiOutlineInbox } from "react-icons/ai";
import { BsFillCameraVideoFill, BsPlus } from "react-icons/bs";
import { LiaBellSolid } from "react-icons/lia";
import { LogoINUI } from "@/components/icons";
import { useSupabaseContext } from "@/services/supabase";
import { LiaSignOutAltSolid, LiaUserCogSolid } from "react-icons/lia";
import { useDrawerContext } from "@/components/drawer";
import { formatDistanceToNow } from "date-fns";
import { AiOutlineUser, AiOutlineUsergroupAdd } from "react-icons/ai";

export default function App() {
  const bg = useColorModeValue("white", "gray.800");
  const mobileNav = useDisclosure();
  const [user, setUser] = useState(null);
  const { handleOpenDrawer } = useDrawerContext();

  const { supabase } = useSupabaseContext();

  useEffect(() => {
    supabase.auth.getSession().then(({ data }) => {
      setUser(data?.session?.user);
    });
  }, []);

  return (
    <React.Fragment>
      <chakra.header bg={bg} w="full" p={2} shadow="md">
        <Flex alignItems="center" justifyContent="space-between" mx="auto">
          <HStack display="flex" spacing={3} alignItems="center">
            <Box display={{ base: "inline-flex", md: "none" }}>
              <IconButton
                display={{ base: "flex", md: "none" }}
                aria-label="Open menu"
                fontSize="20px"
                color="gray.800"
                _dark={{ color: "inherit" }}
                variant="ghost"
                icon={<AiOutlineMenu />}
                onClick={mobileNav.onOpen}
              />
              <VStack
                pos="absolute"
                top={0}
                left={0}
                right={0}
                display={mobileNav.isOpen ? "flex" : "none"}
                flexDirection="column"
                p={2}
                pb={4}
                m={2}
                bg={bg}
                spacing={3}
                rounded="sm"
                shadow="sm"
              >
                <CloseButton
                  aria-label="Close menu"
                  justifySelf="self-start"
                  onClick={mobileNav.onClose}
                />
                <Button w="full" variant="ghost" leftIcon={<AiFillHome />}>
                  Dashboard
                </Button>
                <Button
                  w="full"
                  variant="solid"
                  colorScheme="brand"
                  leftIcon={<AiOutlineInbox />}
                >
                  Inbox
                </Button>
                <Button
                  w="full"
                  variant="ghost"
                  leftIcon={<BsFillCameraVideoFill />}
                >
                  Videos
                </Button>
              </VStack>
            </Box>
            <chakra.a
              href="/"
              title="Choc Home Page"
              display="flex"
              alignItems="center"
            >
              <LogoINUI />
            </chakra.a>

            <HStack spacing={3} display={{ base: "none", md: "inline-flex" }}>
              <Button variant="ghost" size="sm">
                INUI Gaming
              </Button>
              <Button
                variant="solid"
                colorScheme="brand"
                leftIcon={<AiOutlineInbox />}
                size="sm"
              >
                Contact
              </Button>
            </HStack>
          </HStack>
          <HStack
            spacing={3}
            display={mobileNav.isOpen ? "none" : "flex"}
            alignItems="center"
          >
            <Button
              onClick={() => {
                handleOpenDrawer({
                  type: "add-contact",
                });
              }}
              colorScheme="brand"
              sx={{ svg: { fontSize: "24px" } }}
              leftIcon={<AiOutlineUser />}
            >
              Contact
            </Button>

            <Button
              onClick={() => {
                handleOpenDrawer({
                  type: "add-group",
                });
              }}
              colorScheme="brand"
              sx={{ svg: { fontSize: "24px" } }}
              leftIcon={<AiOutlineUsergroupAdd />}
            >
              Group
            </Button>

            <chakra.a
              p={3}
              color="gray.800"
              _dark={{ color: "inherit" }}
              rounded="sm"
              _hover={{ color: "gray.800", _dark: { color: "gray.600" } }}
            >
              <IconButton
                isRound
                fontSize="20px"
                aria-label="LiaBellSolid"
                icon={<LiaBellSolid />}
              />

              <VisuallyHidden>Notifications</VisuallyHidden>
            </chakra.a>

            {user && (
              <Menu>
                <MenuButton>
                  <Avatar
                    size="md"
                    name={user?.email}
                    src={user?.user_metadata?.picture}
                  />
                </MenuButton>

                <MenuList>
                  <MenuItem>
                    <HStack spacing={4}>
                      <Avatar
                        size="md"
                        name={user?.email}
                        src={user?.user_metadata?.picture}
                      />
                      <VStack spacing={0} alignItems={"flex-start"}>
                        <Text fontWeight={600}>{user?.email}</Text>
                        <Text fontSize={"14px"}>
                          {formatDistanceToNow(new Date(user?.created_at))}
                        </Text>
                      </VStack>
                    </HStack>
                  </MenuItem>
                  <MenuDivider />

                  <MenuItem
                    sx={{ svg: { fontSize: "20px" } }}
                    icon={<LiaUserCogSolid />}
                  >
                    Your profile
                  </MenuItem>
                  <MenuDivider />

                  <MenuItem
                    sx={{ svg: { fontSize: "20px" } }}
                    icon={<LiaSignOutAltSolid />}
                    onClick={() => {
                      supabase.auth.signOut();
                    }}
                  >
                    Logout
                  </MenuItem>
                </MenuList>
              </Menu>
            )}
          </HStack>
        </Flex>
      </chakra.header>
    </React.Fragment>
  );
}
