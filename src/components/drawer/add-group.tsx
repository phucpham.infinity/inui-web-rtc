import {
  Button,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  Input,
  useToast,
} from "@chakra-ui/react";
import { IProdContent } from "./types";
import { useEffect, useRef, useState } from "react";
import { useSupabaseContext } from "@/services/supabase";
import Select from "react-select";

const AddContact = (props: IProdContent) => {
  const { onClose } = props;
  const toast = useToast();

  const formEl = useRef<any>(null);
  const { supabase } = useSupabaseContext();
  const [isLoading, setIsLoading] = useState(false);

  const [contacts, setContacts] = useState<any[]>([]);

  const fetchContacts = async () => {
    setIsLoading(true);
    const { data } = await supabase.auth.getSession();
    const { data: _contacts, error } = await supabase
      .from("contacts")
      .select("*")
      .eq("user_id", data.session.user.id);
    if (!error) setContacts(_contacts);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchContacts();
  }, []);

  const handleSaveContact = async ({ name, email, users }: any) => {
    setIsLoading(true);
    const { data } = await supabase.auth.getSession();
    const { error } = await supabase
      .from("contacts")
      .insert([
        {
          user_id: data.session.user.id,
          name,
          email,
          users,
          type: "GROUP",
        },
      ])
      .select();

    if (error) {
      toast({
        title: "Something went wrong!",
        description: `Error: ${error.message}`,
        status: "error",
        isClosable: true,
      });
    } else {
      toast({
        title: "Save contact success!",
        status: "success",
        isClosable: true,
      });
      handleClose();
    }
    setIsLoading(false);
  };

  const handleClose = () => {
    onClose();
  };

  const [usersSelected, setUsersSelected] = useState<any>(null);
  const [groupName, setGroupName] = useState("");

  const handleSubmitContact = () => {
    handleSaveContact({
      name: groupName,
      email: "",
      users: usersSelected,
    });
  };

  return (
    <>
      <DrawerHeader>Create Group</DrawerHeader>
      <DrawerBody>
        <Input
          onChange={(e) => setGroupName(e.target.value)}
          placeholder="Group name"
          mb={3}
        />
        <Select
          isMulti
          name="contact"
          placeholder="Select members"
          options={contacts}
          getOptionLabel={(x) => x.name}
          getOptionValue={(x) => x.id}
          onChange={setUsersSelected}
          styles={{
            option: (styles) => ({
              ...styles,
              color: "black",
            }),
          }}
        />
      </DrawerBody>

      <DrawerFooter>
        <Button onClick={handleClose} variant="outline" mr={3}>
          Cancel
        </Button>
        <Button
          isLoading={isLoading}
          onClick={handleSubmitContact}
          colorScheme="blue"
        >
          Save
        </Button>
      </DrawerFooter>
    </>
  );
};

export default AddContact;
