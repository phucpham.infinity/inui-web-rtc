import {
  Drawer,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
  Text,
} from "@chakra-ui/react";
import { createContext, useContext, useState } from "react";
import { DrawerContextType } from "./types";

import AddContact from "./add-contact";
import AddGroup from "./add-group";

export const DrawerContext = createContext<DrawerContextType | null>(null);

const CONTENT = {
  "add-contact": AddContact,
  "add-group": AddGroup,
};

const DrawerProvider: React.FC<any> = ({ children }: any) => {
  const [openDrawer, setOpenDrawer] = useState(false);
  const [type, setType] = useState("");
  const [payload, setPayload] = useState<any>(null);

  const handleOpenDrawer = ({
    type,
    payload = null,
  }: {
    type: string;
    payload?: any;
    onBeforeClose?: () => any;
  }) => {
    if (type) {
      setType(type);
      setPayload(payload);
      setOpenDrawer(true);
    }
  };
  const handleCloseDrawer = () => {
    setOpenDrawer(false);
    setType("");
    setPayload(null);
  };

  const ComponentContent = CONTENT[type];

  return (
    <DrawerContext.Provider
      value={{
        handleOpenDrawer,
        handleCloseDrawer,
      }}
    >
      {children}
      <Drawer isOpen={openDrawer} placement="right" onClose={handleCloseDrawer}>
        <DrawerOverlay />
        <DrawerContent minW={"500px"}>
          <DrawerCloseButton />
          {ComponentContent && (
            <ComponentContent onClose={handleCloseDrawer} payload={payload} />
          )}
        </DrawerContent>
      </Drawer>
    </DrawerContext.Provider>
  );
};

export const useDrawerContext = () => {
  return useContext(DrawerContext) as DrawerContextType;
};
export default DrawerProvider;
