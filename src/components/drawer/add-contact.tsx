import {
  Button,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  useToast,
} from "@chakra-ui/react";
import { IProdContent } from "./types";
import FormBuilder from "@/components/form-builder";
import { useRef, useState } from "react";
import { useSupabaseContext } from "@/services/supabase";

const AddContact = (props: IProdContent) => {
  const { onClose } = props;
  const toast = useToast();

  const formEl = useRef<any>(null);
  const { supabase } = useSupabaseContext();
  const [isLoading, setIsLoading] = useState(false);
  const handleSubmitContact = () => {
    formEl.current?.submit?.();
  };

  const handleSaveContact = async ({ name, email }: any) => {
    setIsLoading(true);
    const { data } = await supabase.auth.getSession();
    const { error } = await supabase
      .from("contacts")
      .insert([
        {
          user_id: data.session.user.id,
          name,
          email,
          type: "USER",
        },
      ])
      .select();

    if (error) {
      toast({
        title: "Something went wrong!",
        description: `Error: ${error.message}`,
        status: "error",
        isClosable: true,
      });
    } else {
      toast({
        title: "Save contact success!",
        status: "success",
        isClosable: true,
      });
      handleClose();
    }
    setIsLoading(false);
  };

  const handleClose = () => {
    onClose();
  };

  return (
    <>
      <DrawerHeader>Create new contact</DrawerHeader>
      <DrawerBody>
        <FormBuilder
          ref={formEl}
          onSubmit={handleSaveContact}
          formData={[
            {
              name: "name",
              type: "text",
              label: "Contact name",
              validate: (value) => value !== "" || "Name is required",
            },
            {
              name: "email",
              type: "text",
              label: "Email",
              validate: (value) => value.includes("@") || "Email is invalid",
            },
          ]}
        />
      </DrawerBody>

      <DrawerFooter>
        <Button onClick={handleClose} variant="outline" mr={3}>
          Cancel
        </Button>
        <Button
          isLoading={isLoading}
          onClick={handleSubmitContact}
          colorScheme="blue"
        >
          Save
        </Button>
      </DrawerFooter>
    </>
  );
};

export default AddContact;
