export interface IOpenDrawer {
  type: string;
  payload?: any;
  onBeforeClose?: () => any;
}

export type DrawerContextType = {
  message?: React.ReactNode;
  severity?: "success" | "info" | "error" | "warning";
  openDrawer?: boolean;
  handleOpenDrawer?: (payload: IOpenDrawer) => any;
  handleCloseDrawer?: () => any;
};

export interface IProdContent {
  payload?: any;
  onClose?: () => any;
  onBeforeClose?: () => any;
}
