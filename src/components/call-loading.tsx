import { useEffect } from "react";
import { REGISTER_STATES, WS_STATE, useWS } from "@/context";
import {
  Avatar,
  Center,
  HStack,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Spinner,
  Text,
  VStack,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

interface IPeerLoading {
  peer: string[];
  onClose?: () => any;
  isOpen: boolean;
  name?: string;
  email?: string;
}

function PeerLoading(props: IPeerLoading) {
  const { peer, onClose, isOpen, email } = props;
  const { registerState, call, roomId, stop } = useWS();
  const navigate = useNavigate();
  useEffect(() => {
    if (isOpen && registerState === REGISTER_STATES.REGISTERED) {
      if (peer) {
        call({
          peer,
          videoInput: document.getElementById("myVideo"),
        });
      }
    }
  }, [registerState, isOpen]);

  useEffect(() => {
    if (roomId) {
      navigate(`/room/${roomId}`);
    }
  }, [roomId]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={() => {
        onClose();
      }}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalBody>
          <Center minH={"30vh"}>
            <VStack>
              <Avatar
                size={"xl"}
                src={`https://i.pravatar.cc/600?u=${email}`}
              />
              <Text fontWeight={800} fontSize={"24px"}>
                {email}
              </Text>
              <HStack>
                <Text fontWeight={600} fontSize={"18px"}>
                  Calling
                </Text>
                <Spinner size={"sm"} />
              </HStack>
            </VStack>
          </Center>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}

export default PeerLoading;
